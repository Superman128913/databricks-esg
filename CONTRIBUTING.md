We happily welcome contributions to *ESG_SCORING*. We use GitHub Issues to track community reported issues and GitHub Pull Requests for accepting changes.
